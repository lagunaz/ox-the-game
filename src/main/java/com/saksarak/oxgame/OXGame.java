/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saksarak.oxgame;

import java.util.*;

/**
 *
 * @author LagunaZi
 */
public class OXGame {

    static Scanner kb = new Scanner(System.in);
    static String table[][] = {
        {"-", "-", "-"},
        {"-", "-", "-"},
        {"-", "-", "-"}};
    static int turn = 1;
    static boolean endgame = false;

    static void showtable() {
        System.out.println("  1 2 3");
        for (int i = 0; i < 3; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    static void updatetable(int row, int col) {
        if (turn % 2 == 1) {
            if (row < 0 || row > 2 && col < 0 || col > 2) {
                System.out.println("Don't have this area");
                turn--;
            } else if (table[row][col].equals("-")) {
                table[row][col] = "X";
            } else if (table[row][col].equals("X")
                    || table[row][col].equals("O")) {
                System.out.println("It's already placed. Input again");
                turn--;
            }
        } else {
            if (row < 0 || row > 2 && col < 0 || col > 2) {
                System.out.println("Don't have this area");
                turn--;
            } else if (table[row][col].equals("-")) {
                table[row][col] = "O";

            } else if (table[row][col].equals("X")
                    || table[row][col].equals("O")) {
                System.out.println("It's already placed. Input again");
                turn--;

            } else if (row < 0 || row > 2 && col < 0 || col > 2) {
                System.out.println("Don't have this area");

            }
        }
    }

    static void runningGame() {
        if (turn == 10) {
            System.out.println("Tie");
            endgame = true;
            return;
        }
        if (turn % 2 == 1) {
            System.out.println("X turn");
            System.out.println("Please input Row Col: ");
            int row = kb.nextInt() - 1;
            int col = kb.nextInt() - 1;
            updatetable(row, col);
            turn++;
            winCheck();
        } else if (turn % 2 == 0) {
            System.out.println("O turn");
            System.out.println("Please input Row Col: ");
            int row = kb.nextInt() - 1;
            int col = kb.nextInt() - 1;
            updatetable(row, col);
            turn++;
            winCheck();
        }
    }

    static boolean winCheck() {
        if (table[0][0].equals("X") && table[0][1].equals("X") && table[0][2].equals("X")
            || table[0][0].equals("X") && table[1][0].equals("X") && table[2][0].equals("X")
            || table[0][0].equals("X") && table[1][1].equals("X") && table[2][2].equals("X")
            || table[1][0].equals("X") && table[1][1].equals("X") && table[1][2].equals("X")
            || table[2][0].equals("X") && table[2][1].equals("X") && table[2][2].equals("X")
            || table[0][1].equals("X") && table[1][1].equals("X") && table[2][1].equals("X")
            || table[0][2].equals("X") && table[1][2].equals("X") && table[2][2].equals("X")
            || table[2][0].equals("X") && table[1][1].equals("X") && table[0][2].equals("X")) {
            showtable();
            System.out.println("Player X Win ...");
            endgame = true;
        } else if 
            (table[0][0].equals("O") && table[0][1].equals("O") && table[0][2].equals("O")
            || table[0][0].equals("O") && table[1][0].equals("O") && table[2][0].equals("O")
            || table[0][0].equals("O") && table[1][1].equals("O") && table[2][2].equals("O")
            || table[1][0].equals("O") && table[1][1].equals("O") && table[1][2].equals("O")
            || table[2][0].equals("O") && table[2][1].equals("O") && table[2][2].equals("O")
            || table[0][1].equals("O") && table[1][1].equals("O") && table[2][1].equals("O")
            || table[0][2].equals("O") && table[1][2].equals("O") && table[2][2].equals("O")
            || table[2][0].equals("O") && table[1][1].equals("O") && table[0][2].equals("O")) {
            showtable();
            System.out.println("Player O Win ...");
            endgame = true;
        }
        return endgame;

    }

    public static void main(String[] args) {

        System.out.println("Welcome to OX Game");
        do {
            showtable();
            runningGame();
        } while (!endgame);
        System.out.println("bye bye...");
    }

}
